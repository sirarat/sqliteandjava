/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sirarat.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author admin02
 */
public class ConnectDatabase {
    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found!!!");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to open database!!!");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }
    
}
